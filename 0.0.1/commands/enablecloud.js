/**
 * @author Paul Ryan
 */

var folder,
    path,
    tiapp,
    devKey,
    prodKey,
    fs = require("fs"),
    _ = require("underscore"),
    username,
    password;

exports.cliVersion = ">=3.2";
exports.desc = "cloud enabler";
exports.extendedDesc = "Cloud enables new apps created with Titanium CLI";
exports.config = function(logger, config, cli) {
    return {
        // Set to false if the user should be logged in to execute the command
        noAuth : true,
        skipBanner : true,
        options : {
            "project-dir" : {
                "abbr" : 'd',
                "desc" : "Directory containing the project, otherwise the current working directory is assumed."
            },
            "username" : {
                "abbr" : "u",
                "desc" : "Cloud calls require reauthentication"
            },
            "password" : {
                "abbr" : "p",
                "desc" : "Cloud calls require reauthentication"
            }
        }
    };
};
exports.validate = function(logger, config, cli) {
    if (!cli.argv["project-dir"]) {
        logger.log("using current directory");
        folder = process.cwd() + '/';
    } else {
        logger.log("using " + cli.argv["project-dir"]);
        folder = cli.argv["project-dir"];
    }
    path = folder + 'tiapp.xml';
    if (!fs.existsSync(path)) {
        logger.error("Is this a titanium mobile app folder?. No tiapp.xml found");
        logger.error(path);
    } else {
        tiapp = require('tiapp.xml').load(path);
        if (!tiapp) {
            logger.error("Unable to load tiapp.xml");
        } else {
            devKey = tiapp.getProperty('acs-api-key-development');
            prodKey = tiapp.getProperty('acs-api-key-production');
            //if either key exists, app is already cloud enabled
            if (devKey || prodKey) {
                logger.info("App is cloud enabled. Exiting");
            }
        }
    }
};
exports.run = function(logger, config, cli) {
    var async = require('async'),
        prompt = require('prompt'),
        username,
        password,
        properties;

    async.waterfall([
    function(callback) {
        logger.info("Adding cloud keys to app.  Hold on.");
        logger.info("Processing " + tiapp.name + " " + tiapp.guid);

        if (cli.argv.username && cli.argv.password) {
            username = cli.argv.username;
            password = cli.argv.password;
            callback(null, {
                username : username,
                password : password
            });
            //need to prompt the user if not provided
        } else {
            properties = [{
                name : 'username',
                hidden : false,
            }, {
                name : 'password',
                hidden : true
            }];

            prompt.start();

            prompt.get(properties, function(err, result) {
                if (err) {
                    callback("Unable to capture login details", null);
                }
                if (result.username && result.password) {
                    logger.log('  Username: ' + result.username);
                    logger.log('  Password: ' + result.password);
                    callback(null, {
                        username : result.username,
                        password : result.password
                    });
                } else {
                    callback("Unable to capture login details", null);
                }
            });
        }
    },
    function(args, callback) {
        //copy default options over
        var loginOptions = {},
            testString;
        request = require('request');
        loginOptions.url = "https://api.cloud.appcelerator.com/v1/admins/studio_login.json";
        loginOptions.form = {
            "un" : args.username,
            "pw" : args.password
        };
        testString = "un=" + encodeURIComponent(args.username) + "&pw=" + encodeURIComponent(args.password);
        loginOptions.headers = {
            "User-Agent" : "TitaniumStudio/3.4",
            "Accept" : "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2",
            "Connection" : "keep-alive",
            "Content-Length" : "" + testString.length,
            "Content-type" : "application/x-www-form-urlencoded"
        };
        request.post(loginOptions, function(err, response, body) {
            var obj = {
                meta : {}
            };
            obj = JSON.parse(body);
            if (err) {
                callback(err, null);
            }
            callback(null, {
                session : obj.meta.session_id
            });
        });
    },
    function(args, callback) {
        var session,
            enableOptions,
        //needs a cookie
            request = require('request').defaults({
            jar : true
        }),
            j,
            cookie,
            cookieString,
            url,
            testString;
        url = "https://api.cloud.appcelerator.com/v1/apps/create.json";
        session = args.session;
        j = request.jar();
        cookieString = '_session_id=' + session;
        cookie = request.cookie(cookieString);
        j.setCookie(cookie, url);
        enableOptions = {};
        enableOptions.url = url;
        enableOptions.jar = j;
        enableOptions.form = {
            "description" : "",
            "name" : tiapp.name,
            "ti_guid" : tiapp.guid,
            "suffix" : "development,production",
            "ti" : "true"
        };
        testString = "description=&name=" + encodeURIComponent(tiapp.name) + "&ti_guid=" + tiapp.guid + "&suffix=" + encodeURIComponent("development,production") + "&ti=true";
        enableOptions.headers = {
            "User-Agent" : "TitaniumStudio/3.4",
            "Accept" : "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2",
            "Connection" : "keep-alive",
            "Content-Length" : "" + testString.length,
            "Content-type" : "application/x-www-form-urlencoded"
        };

        request.post(enableOptions, function(err, response, body) {
            var obj = {};
            obj = JSON.parse(body);
            if (err) {
                callback(err, null);
            }
            callback(null, obj);
        });
    },
    function(args, callback) {
        var devSettings,
            prodSettings;
        try {
            if (args) {
                if (args.meta && args.meta.status) {
                    if (args.meta.status === "ok") {
                        if (args.response && args.response.apps) {
                            devSettings = args.response.apps[0];
                            prodSettings = args.response.apps[1];
                            //set dev settings
                            tiapp.setProperty("acs-oauth-secret-development", devSettings["oauth_secret"], "string");
                            tiapp.setProperty("acs-oauth-key-development", devSettings["oauth_key"], "string");
                            tiapp.setProperty("acs-api-key-development", devSettings["key"], "string");
                            //set prod settings
                            tiapp.setProperty("acs-oauth-secret-production", prodSettings["oauth_secret"], "string");
                            tiapp.setProperty("acs-oauth-key-production", prodSettings["oauth_key"], "string");
                            tiapp.setProperty("acs-api-key-production", prodSettings["key"], "string");
                            tiapp.write();
                            callback(null, "app updated");
                        } else {
                            throw new Error("API keys not returned");
                        }
                    } else {
                        throw new Error(JSON.stringify(args));
                    }
                }
            } else {
                throw new Error("No session returned " + JSON.stringify(args));

            }
        } catch(ex) {
            callback(ex, null);
        }
    }], function(err, results) {
        if (err) {
            logger.error(err);
        } else {
            logger.info(results);
        }
    });
};